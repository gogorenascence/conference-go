from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    # Create the URL for the request with the city and state
    # Make the request
    # photo_response = requests.get(
    #     f"http://api.pexels.com/v1/search?query={city}+{state}&per_page=1",
    #     params=params,
    #     headers=headers,
    # )
    # # Parse the JSON response
    # photo = json.loads(photo_response.content)
    # # Return a dictionary that contains a `picture_url` key and
    # #   one of the URLs for one of the pictures in the response
    # photo_url = {
    #     "picture_url": photo["photos"][0]["src"]["medium"],
    # }
    # return json.dumps(photo_url)
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None

    # # Create the URL for the geocoding API with the city and state
    # # Make the request
    # location_response = requests.get(
    #     f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=5&appid={OPEN_WEATHER_API_KEY}"
    # )
    # # Parse the JSON response
    # location_dict = json.loads(location_response.content)
    # # Get the latitude and longitude from the response
    # lattitude = location_dict["lat"]
    # longitude = location_dict["lon"]

    # # Create the URL for the current weather API with the latitude
    # #   and longitude
    # # Make the request
    # weather_response = requests.get(
    #     f"https://api.openweathermap.org/data/2.5/weather?lat={lattitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"
    # )
    # # Parse the JSON response
    # weather_dict = json.loads(weather_response.content)
    # # Get the main temperature and the weather's description and put
    # #   them in a dictionary
    # descript = weather_dict["weather"]["description"]
    # temp = weather_dict["main"]["temp"]
    # weather = {
    #     "Description": descript,
    #     "Temperature": temp,
    # }
    # # Return the dictionary
    # return weather
